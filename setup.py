from setuptools import setup, find_packages
from hostwriter import __version__

setup(
    name="hostwriter",
    version=__version__,
    py_modules=["hostwriter"],
    packages=find_packages(),
    include_package_data=True,
    install_requires=["Click", "defusedxml", "httplib2", "requests"],
    entry_points={
        "console_scripts": [
            "hostwriter = hostwriter.hw:cli",
        ],
    },
)
