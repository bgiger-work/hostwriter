import click
import hostwriter.csv_connector
import hostwriter.sat_connector
import hostwriter.nc_connector

reader = None
writer = None


# Command line parsing
@click.group(chain=True, help="Copy hosts from NetCenter to Satellite and back")
def cli():
    pass


@cli.command("csv")
@click.option(
    "--filename",
    "-f",
    required=True,
    help="Use CSV file (columns IP, FDQN, MAC); '-' to write on stdout",
)
def csv(filename):
    global reader
    global writer
    if reader is None:
        reader = hostwriter.csv_connector.CsvConnection(filename)
    else:
        writer = hostwriter.csv_connector.CsvConnection(filename)
        reader.read()
        writer.write(reader.systems)


@cli.command("sat")
@click.option(
    "--satellite_username", "-su", required=True, help="Username for Satellite"
)
@click.option(
    "--satellite_password",
    "-sp",
    required=True,
    prompt=True,
    hide_input=True,
    help="Password for Satellite",
)
@click.option("--organization", "-o", required=True, help="Destination organization")
@click.option("--hostgroup", "-h", required=True, help="Destination hostgroup")
@click.option(
    "--location", "-l", default="Zurich", required=True, help="Satellite location"
)
@click.option(
    "--server", "-s", default="id-sat-prd.ethz.ch", help="Location of capsule"
)
def sat(
    satellite_username, satellite_password, organization, hostgroup, location, server
):
    global reader
    global writer
    if reader is None:
        reader = hostwriter.sat_connector.SatConnection(
            satellite_username,
            satellite_password,
            organization,
            hostgroup,
            location,
            server,
        )
    else:
        writer = hostwriter.sat_connector.SatConnection(
            satellite_username,
            satellite_password,
            organization,
            hostgroup,
            location,
            server,
        )
        reader.read()
        writer.write(reader.systems)


@cli.command("nc")
@click.option(
    "--netcenter_username", "-nu", required=True, help="Username for NetCenter"
)
@click.option(
    "--netcenter_password",
    "-sp",
    required=True,
    prompt=True,
    hide_input=True,
    help="Password for NetCenter",
)
@click.option("--subnet", "-n", required=True, help="Destination subnet in NetCenter")
@click.option("--isg", "-i", required=True, help="ISG to use in NetCenter")
@click.option("--min", help="Minimum IP to export")
@click.option("--max", help="Maximum IP to export")
def nc(netcenter_username, netcenter_password, subnet, isg, min, max):
    global reader
    global writer
    if reader is None:
        reader = hostwriter.nc_connector.NcConnection(
            netcenter_username, netcenter_password, isg, subnet, min, max
        )

    else:
        writer = hostwriter.nc_connector.NcConnection(
            netcenter_username, netcenter_password, isg, subnet
        )
        reader.read()
        writer.write(reader.systems)


if __name__ == "__main__":
    cli()
