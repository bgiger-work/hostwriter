FROM python:3-alpine3.9
# ARG VCS_TAG

LABEL maintainer="Bengt Giger <bgiger@ethz.ch>"
ENV PYTHONUNBUFFERED=1

COPY hostwriter /app/hostwriter
COPY hostwriter/hw.py requirements.txt cli.py /app/

WORKDIR /app
RUN pip install -r requirements.txt
# RUN echo $VCS_TAG >.version.txt

RUN addgroup --system py && adduser --system --ingroup py py
USER py:py

CMD python3 hw.py
