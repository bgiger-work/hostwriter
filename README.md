# Host Info Transfer

Transfer host information between CSV, Satellite 6 and NetCenter. Manage hosts
in a database or spreadsheet and use a CSV to configure Satellite and NetCenter.

## Installation
### Python Package
*hostwriter* may be installed with *pip*. Read the description in the
[local package registry](https://gitlab.ethz.ch/bgiger-work/hostwriter/-/packages) for
installation instructions.

The program will be available as `hostwriter` after installation.

### Docker Image
To use the Docker image:

```
docker run registry.ethz.ch/bgiger-work/hostwriter:latest <options>
```
or any version tag available.

## Handled Data

Data common to Satellite 6 and NetCenter are

 * host name as FQDN (hostname.ethz.ch)
 * MAC address (both formats allowed: AA-bb- or aa:BB)
 * IP address

When using one of these sources, a resulting CSV has the following structure:

```
"<IP number>";"<hostname>";"MAC address"
```

A CSV file produced externally i.e. by a database has to contain these fields,
in this order.

## Credentials
As NetCenter and Satellite rarely share the same credentials, you have to
provide a double set of logins and passwords.

If no passwords are supplied, you will be asked to type them in into the
terminal.

Passwords may also be supplied as environment variables `SAT_PASSWORD` and `NC_PASSWORD`.

# Syntax and Examples

General usage pattern:

`hostwriter <SOURCE> <SOURCE_OPTIONS> <TARGET> <TARGET_OPTIONS>`

where SOURCE and TARGET may be `csv`, `sat` or `nc`.

## Test your CSV

Does nothing but read a CSV, parse it and dump it to the terminal:

`hostwriter csv -f input.csv csv -f -`

## Generate CSV with IP range from NetCenter

Read an IP range from NetCenter and write hosts to a CSV

`hostwriter nc -nu bgiger -n 129.132.179.0 -i id --min 129.132.179.2 --max 129.132.179.10 csv -f hosts.csv`

## Write previously generated CSV to Satellite

Write the hosts in Satellite:

`hostwriter csv -f hosts.csv sat -su bgi4tst -o ID-CD -h TestClients`

Notes:
- Host may not exists, there is no update functionality
- Hosts with MAC address will be put in *managed* state. Without MAC, they will end up in *unmanaged* state

## Configure NetCenter from CSV

`hostwriter -f input.csv nc -nu bgiger -n 129.132.179.0 -i id-cd-linux`
