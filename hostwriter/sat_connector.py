import logging
import requests
import hostwriter.config_objects


class SatConnection:
    """
    Connect to a Satellite (foreman) server and read or write hosts.

    API documentation can be read at <satellite-url>/apidoc
    """

    SSL_VERIFY = False
    API = "api/v2"
    SAT_API = "katello/api/v2"
    POST_HEADERS = {
        "content-type": "application/json",
        "Accept": "application/json,version=2",
    }

    def __init__(
        self,
        username,
        password,
        organization,
        hostgroup,
        location="Zurich",
        server="id-sat-prd.ethz.ch",
    ):
        """
        Create a new connection object.

        :param username: Username authorized in Satellite
        :param password: Guess...
        :param organization: Create host(s) in this organization
        :param hostgroup: Assign created hosts to this host group
        :param location: (mandatory) Location of capsule, defaults to 'Zuerich'
        :param server: (mandatory) Defaults to id-sat-prd.ethz.ch
        """
        self.username = username
        self.password = password
        self.server = server
        self.organization = organization
        self.location = location
        self.hostgroup = hostgroup
        self.systems = hostwriter.config_objects.SystemList()

    def get_json(self, url):
        """
        Send GET request to Satellite

        :param url: GET url sent to Satellite API
        :return: JSON returned or HTTPError object
        """
        r = requests.get(url, auth=(self.username, self.password), timeout=30)
        if r.status_code == requests.codes.ok:
            return r.json()
        else:
            r.raise_for_status()

    def post_json(self, url, json_data):
        """
        Send POST request to Satellite

        :param url: GET url sent to Satellite API
        :return: JSON returned or HTTPError object
        """
        r = requests.post(
            url,
            json=json_data,
            auth=(self.username, self.password),
            # verify=SatConnection.SSL_VERIFY,
            headers=SatConnection.POST_HEADERS,
            timeout=30,
        )
        if r.status_code == requests.codes.ok:
            return r.json()
        else:
            logging.error("Error in response: {0}".format(json_data))
            r.raise_for_status()

    def hostgroup_id(self):
        """
        Convert host group name to ID

        :return: Numerical ID for request
        """
        hostgroups_raw = self.get_json(
            "https://{0}/{1}/hostgroups".format(self.server, SatConnection.API)
        )
        for group in hostgroups_raw["results"]:
            if group["name"] == self.hostgroup:
                return group["id"]
        logging.error("Hostgroup not found")
        exit(1)

    def location_id(self):
        """
        Convert location name to ID

        :return: Numerical ID for request
        """
        locations_raw = self.get_json(
            "https://{0}/{1}/locations".format(self.server, SatConnection.API)
        )
        for location in locations_raw["results"]:
            if location["name"] == self.location:
                return location["id"]
        logging.error("Location not found")
        exit(1)

    def organization_id(self):
        """
        Convert organization name to ID

        :return: Numerical ID for request
        """
        organizations_raw = self.get_json(
            "https://{0}/{1}/organizations".format(self.server, SatConnection.API)
        )
        for organization in organizations_raw["results"]:
            if organization["name"] == self.organization:
                return organization["id"]
        logging.debug("Organization not found")
        exit(1)

    def read(self):
        """
        Read host list from Satellite
        """
        # https://satellite6.example.com/api/v2/hosts?search=example
        hosts_raw = self.get_json(
            'https://{0}/{1}/hosts?search=hostgroup_fullname+%3D+"{2}"'.format(
                self.server, SatConnection.API, self.hostgroup
            )
        )

        for item in hosts_raw["results"]:
            host = hostwriter.config_objects.System(
                item["ip"], item["name"], item["mac"]
            )
            self.systems.append(host)

    def write(self, systems):
        """
        Write hosts to Satellite
        :param systems: Systems list from a reading connector
        """
        try:
            for host in systems:
                hostdict = host.sat_struct()
                hostdict["host"]["hostgroup_id"] = self.hostgroup_id()
                hostdict["host"]["location_id"] = self.location_id()
                hostdict["host"]["organization_id"] = self.organization_id()
                self.post_json(
                    "https://%s/%s/hosts/" % (self.server, SatConnection.API), hostdict
                )
        except requests.exceptions.ConnectionError:
            logging.error(
                "Failed to connect to Satellite server {0}".format(self.server)
            )
            exit(1)
