import unittest
import tempfile
import os.path
from hostwriter.csv_connector import CsvConnection
from hostwriter.config_objects import System, SystemList


class ReadCsv(unittest.TestCase):
    def test_none_file(self):
        source = CsvConnection(None)
        self.assertRaises(FileNotFoundError, source.read)

    def test_nonexisting_file(self):
        source = CsvConnection("nonexisting")
        self.assertRaises(FileNotFoundError, source.read)

    def test_not_csv_file(self):
        source = CsvConnection("hostwriter/test_resources/not.csv")
        self.assertRaises(SyntaxError, source.read)

    #    def test_bad_ip(self):
    #        source = CsvConnection("hostwriter/test_resources/bad.csv")
    #        self.assertRaises(SyntaxError, source.read)

    def test_read_good_csv(self):
        source = CsvConnection("hostwriter/test_resources/good.csv")
        source.read()
        self.assertTrue(len(source.systems) == 2)

    def test_read_good_no_mac_csv(self):
        source = CsvConnection("hostwriter/test_resources/good_no_mac.csv")
        source.read()
        self.assertTrue(len(source.systems) == 2)


class WriteCsv(unittest.TestCase):
    def write_dir_invalid(self):
        target = CsvConnection("non existent path")
        target.write()

    def write_verify_csv(self):
        test_file_name = "test.csv"
        systems = SystemList()
        systems.append(
            System("192.168.0.5", "testhost1.example.com", "00:11:22:33:44:55")
        )
        systems.append(
            System(
                "2a00:1450:400a:802::2003", "testhost1.example.com", "00:11:22:33:44:55"
            )
        )
        with tempfile.TemporaryDirectory() as tmp_name:
            target = CsvConnection(os.path.join(tmp_name, test_file_name))
            target.write(systems)

            source = CsvConnection(os.path.join(tmp_name, test_file_name))

        source.read()
        self.assertTrue(len(source.systems) == 2)
