import unittest
from hostwriter.config_objects import System, SubnetList


class TestSystemAddress(unittest.TestCase):
    # def test_system_invalid_ip(self):
    #     with self.assertRaises(ValueError):
    #         System("bogus", "test.example.com", "")

    def test_system_valid_ipv4(self):
        ip = "129.132.95.57"
        host = System(ip, "test.example.com", "")
        self.assertEqual(ip, str(host.ip))

    def test_system_valid_ipv6(self):
        ip = "2001:67c:10ec:42c2::57"
        host = System(ip, "test.example.com", "")
        self.assertEqual(ip, str(host.ip))


class TestSystemOutput(unittest.TestCase):
    def setUp(self):
        self.host_managed = System(
            "129.132.95.57", "test.example.com", "00:00:00:00:00:00"
        )
        self.host_unmanaged1 = System("129.132.95.57", "test.example.com", None)
        self.host_unmanaged2 = System("129.132.95.57", "test.example.com", "")

    def test_system_csv(self):
        self.assertEqual(
            self.host_managed.csv(),
            '"129.132.95.57";"test.example.com";"00:00:00:00:00:00"',
        )

    def test_system_sat_managed(self):
        s = self.host_managed.sat_struct()
        self.assertEqual(str(s["host"]["ip"]), "129.132.95.57")
        self.assertEqual(s["host"]["name"], "test.example.com")
        self.assertEqual(s["host"]["mac"], "00:00:00:00:00:00")

    def test_system_sat_unmanaged(self):
        s = self.host_unmanaged1.sat_struct()
        self.assertEqual(s["host"]["managed"], 0)
        s = self.host_unmanaged2.sat_struct()
        self.assertEqual(s["host"]["managed"], 0)


class TestSystemList(unittest.TestCase):
    def setUp(self):
        self.list = SubnetList("192.168.1.0/24")
        self.list.append(System("192.168.1.11", "test1.example.com", ""))
        self.list.append(System("192.168.1.12", "test2.example.com", ""))
        self.list.append(System("192.168.1.13", "test3.example.com", ""))

    def test_csv_output(self):
        expected_output = """"192.168.1.11";"test1.example.com";""
"192.168.1.12";"test2.example.com";""
"192.168.1.13";"test3.example.com";""
"""

        self.assertEqual(self.list.csv(), expected_output)

    def test_list_limits(self):
        self.assertEqual(len(self.list), 3)

        self.list.min = "192.168.1.12"
        list = self.list.limit()
        self.assertEqual(len(list), 2)

        self.list.min = "192.168.1.12"
        list = self.list.limit()
        self.assertEqual(len(list), 2)

        self.list.min = None
        self.list.max = "192.168.1.12"
        list = self.list.limit()
        self.assertEqual(len(list), 2)

        self.list.min = "192.168.1.12"
        self.list.max = "192.168.1.12"
        list = self.list.limit()
        self.assertEqual(len(list), 1)
