import unittest
import requests
from hostwriter.sat_connector import SatConnection


class ConnectSat(unittest.TestCase):
    def test_unknown_server(self):
        source = SatConnection(
            "unknown", "unknown", "org", "hostgroup", server="unknown.example.com"
        )
        self.assertRaises(requests.exceptions.ConnectionError, source.read)
