import sys
import csv
from hostwriter.config_objects import System, SystemList
import logging


def write_systems(systems, file):
    for system in systems.limit():
        file.write(system.csv())
        file.write("\n")


class CsvConnection:
    def __init__(self, filename):
        self.filename = filename
        self.systems = SystemList()

    def read(self):
        system = None
        if self.filename == "-":
            csv_file = sys.stdin
        else:
            try:
                csv_file = open(self.filename)
            except TypeError as e:
                logging.error("Not a file object to open")
                raise FileNotFoundError(e)

        try:
            rdr = csv.reader(csv_file, delimiter=";")
        except SyntaxError:
            logging.error("Invalid CSV file format")
            csv_file.close()
            exit(1)

        for row in rdr:
            line_ok = False
            try:
                system = System(row[0], row[1], row[2])
                line_ok = True
            except IndexError:
                # Maybe 3rd column is missing?
                pass
            except ValueError:
                logging.error("Invalid address")
                raise SyntaxError

            if not line_ok:
                try:
                    system = System(row[0], row[1], None)
                except IndexError:
                    logging.error("Invalid CSV file format")
                    csv_file.close()
                    raise SyntaxError("Invalid CSV file format")
                except ValueError:
                    logging.error("Invalid address")
                    raise SyntaxError

            self.systems.append(system)
        if self.filename != "-":
            csv_file.close()

    def write(self, systems):
        if self.filename == "-":
            csv_file = sys.stdout
        else:
            csv_file = open(self.filename, mode="w")
        csv_file.write(systems.csv())
        # write_systems(systems, csv_file)
        if self.filename != "-":
            csv_file.close()
