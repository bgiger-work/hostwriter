import ipaddress
import logging
import sys


class System:
    """
    Representation of a system
    """

    def __init__(self, ip, fqdn, mac=None):
        """
        Create a system
        :param ip: IP address (IPv4 or IPv6)
        :param fqdn: fully qualified host name
        :param mac: (optional) MAC address
        """
        try:
            self.ip = ipaddress.ip_address(ip)
        except ValueError:
            logging.error("Invalid IP address")
            self.ip = ipaddress.ip_address("0.0.0.0")  # nosec B104
        self.fqdn = fqdn
        self.mac = mac

    def __repr__(self):
        return "{0} (IP: {1}, MAC: {2})".format(self.ip, self.fqdn, self.mac)

    def address(self):
        """
        Get IP address without subnet prefix
        :return: plain IP address
        """
        return str(self.ip).rsplit("/", 1)[0]

    def csv(self):
        """
        Output host as CSV row
        :return: formatted row string
        """
        if self.mac is not None:
            return '"{0}";"{1}";"{2}"'.format(self.ip, self.fqdn, self.mac)
        else:
            return '"{0}";"{1}";""'.format(self.ip, self.fqdn)

    def sat_struct(self):
        """
        Output host as JSON structure for Satellite API request
        :return:
        """
        if self.mac is not None and self.mac != "":
            sst = {"ip": self.address(), "name": self.fqdn, "mac": self.mac}
        else:
            sst = {"ip": self.address(), "name": self.fqdn, "managed": 0}
        return {"host": sst}


class SystemList(list):
    """
    Base class for a list of systems
    """

    def __init__(self):
        super().__init__()

    def limit(self):
        """
        Base method of filtered list of hosts
        :return: Unfiltered list
        """
        return self

    def csv(self, file=sys.stdout):
        """
        Output all hosts in CSV format
        :return:
        """
        output = ""
        for item in self:
            # print(item.csv())
            output = output + item.csv() + "\n"

        return output


class SubnetList(SystemList):
    """
    List of hosts represented with hosts as subnet members
    """

    def __init__(self, addr, min=None, max=None):
        """
        :param addr: subnet address
        :param min: minimum IP address
        :param max: maximum IP address
        """
        SystemList.__init__(self)
        self.min = min
        self.max = max
        if "." in addr:
            self.subnet = ipaddress.IPv4Network(addr)
        else:
            self.subnet = ipaddress.IPv6Network(addr)
        self.address = str(self.subnet).rsplit("/", 1)[0]

    def limit(self):
        """
        Limit list within minimum and maximum address
        :return: Filtered list
        """
        valid_systems = SystemList()
        for item in self:
            if self.min and item.ip < ipaddress.ip_address(self.min):
                continue
            if self.max and item.ip > ipaddress.ip_address(self.max):
                continue
            valid_systems.append(item)
        return valid_systems


class HostGroupList(SystemList):
    """
    Host list representing a list of hosts in a Satellite host group
    """

    def __init__(self, host_group):
        """
        :param host_group: Host group name in Satellite
        """
        super().__init__()
        self.host_group = host_group
