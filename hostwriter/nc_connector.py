import base64
import httplib2
import logging
import defusedxml.minidom

import hostwriter.config_objects


HOSTINSERTv4 = """
<insert>
  <nameToIP>
    <ip>{ip}</ip>
    <fqName>{fqdn}</fqName>
    <forward>Y</forward>
    <reverse>Y</reverse>
    <ttl>600</ttl>
    <dhcp>Y</dhcp>
    <dhcpMac>{mac}</dhcpMac>
    <ddns>N</ddns>
    <isgGroup>{isg}</isgGroup>
  </nameToIP>
</insert>
"""
HOSTINSERT_NO_MACv4 = """
<insert>
  <nameToIP>
    <ip>{ip}</ip>
    <fqName>{fqdn}</fqName>
    <forward>Y</forward>
    <reverse>Y</reverse>
    <ttl>600</ttl>
    <dhcp>Y</dhcp>
    <ddns>N</ddns>
    <isgGroup>{isg}</isgGroup>
  </nameToIP>
</insert>
"""

HOSTINSERTv6 = """
<insert>
  <nameToIP>
    <ipv6>{ip}</ipv6>
    <fqName>{fqdn}</fqName>
    <forward>Y</forward>
    <reverse>N</reverse>
    <ttl>600</ttl>
    <dhcp>Y</dhcp>
    <dhcpMac>{mac}</dhcpMac>
    <ddns>N</ddns>
    <isgGroup>{isg}</isgGroup>
  </nameToIP>
</insert>
"""
HOSTINSERT_NO_MACv6 = """
<insert>
  <nameToIP>
    <ipv6>{ip}</ipv6>
    <fqName>{fqdn}</fqName>
    <forward>Y</forward>
    <reverse>N</reverse>
    <ttl>600</ttl>
    <dhcp>Y</dhcp>
    <ddns>N</ddns>
    <isgGroup>{isg}</isgGroup>
  </nameToIP>
</insert>
"""


def get_text(nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return "".join(rc)


class NcConnection:
    BASEURL = "https://www.netcenter.ethz.ch/netcenter/rest"
    BASEURL_GETv4 = "%s/%s" % (BASEURL, "nameToIP/usedIps/v4")
    BASEURL_POST = "%s/%s" % (BASEURL, "nameToIP")

    def __init__(self, username, password, isg, subnet, min=None, max=None):
        self.isg = isg
        pw_string = "%s:%s" % (username, password)
        pw_bytes = bytes(pw_string, "utf-8")
        encoded_pw = base64.b64encode(pw_bytes)

        encoded = "Basic %s" % str(encoded_pw, "utf-8")
        self.headers = {"Authorization": encoded, "Content-Type": "text/xml"}

        self.subnet = subnet
        self.systems = hostwriter.config_objects.SubnetList(subnet, min, max)

    def read(self):
        http = httplib2.Http()
        url = "%s/%s" % (NcConnection.BASEURL_GETv4, self.subnet)

        response, content = http.request(url, method="GET", headers=self.headers)
        if response["status"] != "200":
            return []
        else:
            # dom = xml.dom.minidom.parseString(content)
            dom = defusedxml.minidom.parseString(content)

            for ipEntry in dom.getElementsByTagName("usedIp"):
                ip = ipEntry.getElementsByTagName("ip")
                fqname = ipEntry.getElementsByTagName("fqname")

                try:
                    mac = get_text(
                        ipEntry.getElementsByTagName("dhcpMac")[0].childNodes
                    )
                except Exception:
                    mac = ""

                self.systems.append(
                    hostwriter.config_objects.System(
                        get_text(ip[0].childNodes), get_text(fqname[0].childNodes), mac
                    )
                )
        self.systems = self.systems.limit()

    def write(self, systems):
        http = httplib2.Http()
        url = NcConnection.BASEURL_POST
        for host in systems:
            if host.mac is not None and len(host.mac) > 0:
                if "." in host.address():
                    payload = HOSTINSERTv4.format(
                        ip=host.address(), fqdn=host.fqdn, mac=host.mac, isg=self.isg
                    )
                else:
                    payload = HOSTINSERTv6.format(
                        ip=host.address(), fqdn=host.fqdn, mac=host.mac, isg=self.isg
                    )
            else:
                if "." in host.address():
                    payload = HOSTINSERT_NO_MACv4.format(
                        ip=host.address(), fqdn=host.fqdn, isg=self.isg
                    )
                else:
                    if "." in host.address():
                        payload = HOSTINSERT_NO_MACv6.format(
                            ip=host.address(), fqdn=host.fqdn, isg=self.isg
                        )

            response, content = http.request(
                url, method="POST", headers=self.headers, body=payload
            )
            if response["status"] == "401":
                logging.error("NetCenter credentials failure")
                exit(1)

            dom = defusedxml.minidom.parseString(content)
            msg = dom.getElementsByTagName("success")
            if len(msg) > 0:
                logging.info("{0}: ok".format(host.address()))

            # no success message, ergo error
            for part in dom.getElementsByTagName("msg"):
                logging.error(get_text(part.childNodes))
                # exit(1)
